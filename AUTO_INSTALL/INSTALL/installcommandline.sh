#!/bin/bash
# This is for systems that have no whiptail installed.
# Also, whiptail is hard to use....
# So now we start
echo "<Installer> This installer will turn your $hostname into a mjpg-streamer!"
echo "This is an easy install so all you have to do is sit back and relax."
echo "We will start now. Make sure you have run this as root, otherwise ctrl-c this NOW and run this as root."
echo "Copy and paste this(if you didn't run this as root): curl -L jraspiprojects.github.io/mjpg-streamer-easy/installcmdline.sh | sudo bash"
echo "We will begin."
apt-get -y install libjpeg8-dev imagemagick libv4l-dev
ln -s /usr/include/linux/videodev2.h /usr/include/linux/videodev.h
wget https://github.com/jraspiprojects/mjpg-streamer-easy/raw/master/mjpg-streamer-code-182.zip
unzip mjpg-streamer-code-182.zip
cd mjpg-streamer-182/mjpg-streamer
make mjpg-streamer input_file.so output_http.so
cp mjpg-streamer /usr/local/bin
cp output_http.so input_file.so /usr/local/lib
cp -R www /usr/local/www
function InstallVGA(){
	sudo mkdir /tmp/stream && raspistill --nopreview -w 640 -h 480 -q 5 -o /tmp/stream/pic.jpg -tl 100 /t 9999999 -th 0:0:0 & > mjpg-streamer-easy
	cp mjpg-streamer-easy /sbin/
}# need to figure out some things. Don't use the -v flag! mjpg-streamer won't work!
function main_install(){
	echo 'sudo LD_LIBRARY_PATH=/usr/local/lib mjpg-streamer -i "input_file.so -f  /tmp/stream -n pic.jpg" -o "output_file.so -w /usr/local/www"' > mjpg-streamer-easy
	cp mjpg-streamer-easy /sbin/
}
if [ "$1" == "-v" ]; then
    echo "VGA is enabled. Using VGA for install."

else
    echo "VGA is not enabled. Skipping."
    main_install
fi
echo "Install is Finished! Use the mjpg-streamer-easy command to start your streaming interface."

 

